
# Base Mobile Holonome

Code Arduino pour la base mobile holonome à 4 roues omnidirectionnelles, pour la carte NUCLEO F446RE.



## A faire

* Commande pour modifier les paramètres des PID des moteurs
* Asservissement en position V2, esquivement d'obstacle et de bords de terrain



## Utilisation

La communication avec la carte NUCLEO F446RE se fait par liaison série suivant le protocole USART.
Cette liaison se fait avec un Baudrate de 115200, cette vitesse est modifiable dans le fichier `base_mobile.ino`.

Chaque transmission se termine dans les deux sens par un saut de ligne, noté `\n` dans cette documentation.


La commande `D` donnera une réponse immédiate, et la commande `P` donnera une réponse lorsque la base sera arrivée à la position désirée.

Les crochets `[]` entourent des paramètres facultatifs, ils ne sont pas à inclure dans la trame de commande.

Les accolades `{}` entourent les variables de commandes, elles ne sont pas à inclure dans la trame de commande.

Chaque commande contiendra en second paramètre un UUID, généré par la Raspberry. Si un UUID n'est pas nécessaire, il peut être vide, mais le nombre de séparateur doit rester le même.
### Configurations

Le paramétrage des constantes correspondant au rayon des roues ainsi qu'au "rayon" de la base (la distance entre le centre de la base et les roues) se fait avec la commande `Y`, où `R_MM` et `L_MM` sont respectivement le rayon des roues et le rayon de la base, en millimètres.


| Commande            |
| :------------------ |
| `A;{UUID};{R_MM};{L_MM}\n` |


Le paramétrage des constantes correspondant à la vitesse maximale et à l'accélération de la base pour la commande en position se fait avec la commande `B`, où `VITESSEMAX` et `ACCEL` sont respectivement la vitesse maximale en mm/s et l'accélération de la base en mm/s².

| Commande                   |
| :------------------------- |
| `B;{UUID};{VITESSEMAX};{ACCEL}\n` |
### Consigne en vitesse directe absolue

La consigne de vitesse est directement envoyée au moteurs.
Peut générer des pics de courant et/ou des glissements non désirés si la consigne change brusquement.

| Commande            |
| :------------------ |
| `V;{UUID};{VX};{VY};{W}\n` |

* `VX` est la vitesse dans l'axe X du terrain, en mm/s
* `VY` est la vitesse dans l'axe Y du terrain, en mm/s
* `W` est la vitesse de rotation de la base, en rad/s
### Consigne en vitesse directe relative

La consigne de vitesse relative déplace la base selon des axes locaux, et non les axes du terrain.

| Commande            |
| :------------------ |
| `VL;{UUID};{VX};{VY};{W};{Alpha}\n` |

* `VX` est la vitesse dans l'axe X local, en mm/s
* `VY` est la vitesse dans l'axe Y local, en mm/s
* `W` est la vitesse de rotation de la base, en rad/s
* `Alpha` est l'angle entre les axes de la base et les axes locaux du déplacement, en Deg/s

### Consigne en vitesse filtrée absolue

La consigne en vitesse est filtrée en interne, puis envoyée au moteurs.
Evite les pics de courants, mais répond plus lentement. Utile pour des démonstrations.

| Commande            |
| :------------------ |
| `F;{UUID};{VX};{VY};{W}\n` |

* `VX` est la vitesse dans l'axe X du terrain, en mm/s
* `VY` est la vitesse dans l'axe Y du terrain, en mm/s
* `W` est la vitesse de rotation de la base, en rad/s

### Consigne en vitesse filtrée relative

La consigne de vitesse filtrée relative déplace la base selon des axes locaux, et non les axes du terrain. Les vitesses sont filtrées.

| Commande            |
| :------------------ |
| `FL;{UUID};{VX};{VY};{W};{Alpha}\n` |

* `VX` est la vitesse dans l'axe X local, en mm/s
* `VY` est la vitesse dans l'axe Y local, en mm/s
* `W` est la vitesse de rotation de la base, en rad/s
* `Alpha` est l'angle entre les axes de la base et les axes locaux du déplacement, en Deg/s

### Consigne en position

La consigne en position se fait en deux étapes : 
* Envoi de la position consigne
* Attente de la fin du mouvement

| Commande                     | Réponse quand arrivé | 
| :--------------------------- | :------------------- |
| `P;{UUID};{X};{Y};{Alpha}\n` | `P;{UUID};Done\n`    |

* `X` est la position dans l'axe X du terrain, en mm
* `Y` est la position dans l'axe Y du terrain, en mm
* `Alpha` est l'angle de la base dans le sens trigonométrique, en radians

### Calibration de la position de la base

Cette commande calibre la position de la base, elle se fait à l'arrêt. 

Sert à corriger l'erreur de la position de la base, ou à définir la position de départ.


| Commande              |
| :-------------------- |
| `C;{UUID};{X};{Y};{Alpha}\n` |

* `X` est la position dans l'axe X du terrain, en mm
* `Y` est la position dans l'axe Y du terrain, en mm
* `Alpha` est l'angle de la base dans le sens trigonométrique, en radians

### Arrêt de la base

Cette commande arrête complètement la base, et laisse les moteurs à vide

(non implémentée)

| Commande        |
| :-------------- |
| `S;{UUID}\n`    |

### Demande données

Cette commande demande à la base mobile sa position en mémoire

| Commande     | Réponse                         |
| :----------- | :------------------------------ |
| `D;{UUID}\n` | `D;{UUID};{X};{Y};{Alpha}\n`    |

* `X` est la position dans l'axe X du terrain, en mm
* `Y` est la position dans l'axe Y du terrain, en mm
* `Alpha` est l'angle de la base dans le sens trigonométrique, en radians
### Obstacles

Cette commande indique à la base si un obstacle est présent ou non.
Dans cette implémentation, le robot s'arrête momentanément s'il y a présence, et reprend si l'obstacle est parti.

Dans une future implémentation, cette commande indiquera la position des obstacles afin de les esquiver.

| Commande                |
| :---------------------- |
| `O;{UUID};{Presence}\n` |

* `Presence` est `1` si un obstacle est proche, `0` sinon.

### Prise gateaux (spécifique à robot1)

Cette commande pilote les servomoteurs qui attrappent les piles de gâteaux via un esclave I2C.


| Commande                |
| :---------------------- |
| `G;{UUID};{Angle};{Angle};{Angle};{Angle}\n` |

* `Angle` est un angle entre 90 et 180 degrés, un pour chaque "grip".

### Aspiration via ESC (spécifique à robot2)

Cette commande pilote l'ESC qui alimente la turbine d'aspiration de cerises.


| Commande                |
| :---------------------- |
| `E;{UUID};{Actif}\n` |

* `Actif` est `1` pour allumer la turbine, `0` pour l'éteindre.

### Trappe de relâchement des cerises (spécifique à robot2)

Cette commande pilote le servomoteur relié à la trappe de relâchement.


| Commande                |
| :---------------------- |
| `T;{UUID};{Actif}\n` |

* `Actif` est `1` pour ouvrir la trappe, `0` pour la refermer.

## Exemple d'utilisation

```
>>> C;un_uuid_aleatoire;200;200;0\n                 // Calibration sur la position initiale

>>> P;un_autre_uuid_aleatoire;1200;2000;1.57\n        //X = 1.2m, Y = 2m, Alpha = PI/2 rad

<<< P;un_autre_uuid_aleatoire;Done\n                //La base est arrivé, l'UUID est la même que celle reçue

>>> D;;\n                               //Ici un exemple d'une commande avec un UUID vide
<<< D;;1999.8272;2000.5145;89.7086\n    //Valeurs de retour suivant la précision demandée

>>> S;encore_un_autre_uuid_aleatoire\n

```