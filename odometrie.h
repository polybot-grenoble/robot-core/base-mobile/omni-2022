#ifndef __odometrie__
#define __odometrie__
#include <Arduino.h>

#define NMOTORS 4

#define CONSTDEBUT 0

#define STOP_FACTOR 0.5

//consigne en vitesse
typedef struct {
    float x = 0, y = 0, w = 0, alpha = 0;
} TVitesse;


//consigne en position
typedef struct {
    float x = 0, y = 0, alpha = 0;
} TPosition;

typedef struct {
    float x = 0, y = 0;
} TAccel;

typedef enum {NOTFOUND, FOUND, FOUND_FORCED, BLOCKED} PathStatus;
typedef enum {NOTEXPLORED, EXPLORED, BLOCKING, FULLYBLOCKED} Obs_status;


void changeParams(float v, float a);
void changeParams2(float l, float r);
void changeObstacles(int nb, TPosition *obs);
void odometrieLocale(float posDiff[], float *dx, float *dy, float *dw);
void calculCommandeAbsolu2(float Vitesse, float Angle, float w, float alpha, float target[]);
void calculCommandeAbsolu(TVitesse consigne, float alpha, float target[]);
void calculCommandeAbsolu(float vx, float vy, float w, float alpha, float target[]);
bool isInside(TPosition point);
bool isInside(float x, float y);
int calculCommandePosition(TPosition commande_position, TPosition position, TVitesse *vitesse, float precision, float precisionAngle, float deltaT, float target[]);
#endif