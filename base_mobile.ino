#include "PID.cpp"
#include "odometrie.h"

// emulation du robot, sert à déboguer un autre programme (ex. Superviseur) si le robot n'est pas disponible
// décommenter la ligne suivante pour l'émulation
//#define EMULATION

#ifdef EMULATION
#pragma message("The program is in EMULATION mode")
#endif

// Code spécifique robot1
#include <Wire.h>

#define slave_address 1 //adresse de l'esclave, à vérifier dans le code du slave


//Code spécifique robot2
#include <Servo.h>
#define pinPilotage_trappe PC8
#define pinPilotageESC PB7
#define ImpulsionESC_MIN 1000
#define ImpulsionESC_MAX 2000

#define ESC_ON 1400
#define ESC_OFF 1000

//constantes à calibrer
#define TRAPPE_OUVERT 20
#define TRAPPE_FERMEE 90 




#define NB_MOT 30

// Pins (défini d'ordre des moteurs)
const int enca[] = {PC0, PA9, PB13, PA10};
const int encb[] = {PC1, PC7, PB14, PB3 };
const int  pwm[] = {PB0, PB6, PB15, PB4 };
const int  in1[] = {PA1, PA6, PB2 , PB10};
const int  in2[] = {PA4, PA7, PB1 , PA8 };

// Définition du type de commande
typedef enum {M_IDLE, M_VITESSE, M_VITESSE_FILTRE, M_POSITION} TMode_Commande;
typedef struct {
    TMode_Commande commande = M_IDLE;
    char local = 0;
} TMODE;


// Globals
long prevT = 0;
int posPrevi[] = {0, 0, 0, 0};
volatile int posi[] = {0, 0, 0, 0};
float target[NMOTORS];

// position en mémoire
TPosition position;
TVitesse vitesse;

// Communication
String inputString[NB_MOT+1];   
bool stringComplete = false;   
int numero_string=0;

// coeff filtre de commande vitesse mode filtre
float coeffFiltre = 0.003;

volatile TMODE mode;
int arrivee = 0;

// mémoire filtres vitesses des moteurs
float vFilt[] = {0, 0, 0, 0};
float vPrev[] = {0, 0, 0, 0};

// PID moteurs
SimplePID pid[NMOTORS];

// commandes
TVitesse commande_vitesse;
TPosition commande_position;
TAccel accel;

String UUID;


Servo ESC;

Servo servo_deguisement;

Servo servo_trappe;

#ifdef EMULATION
float posEmul[NMOTORS];
#endif


/******************/
/* INITIALISATION */
/******************/
void setup() {

  Serial.begin(115200); 
  Wire.begin();

  pinMode(LED_BUILTIN, OUTPUT);

  servo_trappe.attach(pinPilotage_trappe);
  servo_trappe.write(TRAPPE_FERMEE);

  ESC.attach(pinPilotageESC,ImpulsionESC_MIN,ImpulsionESC_MAX);
  ESC.writeMicroseconds(1000);  //armement du moteur



  // initialisation de la communication
  for(int i=0;i<=(NB_MOT+1);i++){
    inputString[i].reserve(40);
  }
  for(int i=0;i<=(NB_MOT+1);i++){
      inputString[i]="";
  }
  numero_string = 0;
  
  // initialisation des moteurs
  for(int k = 0; k < NMOTORS; k++){
    pinMode(enca[k],INPUT);
    pinMode(encb[k],INPUT);
    pinMode(pwm[k],OUTPUT);
    pinMode(in1[k],OUTPUT);
    pinMode(in2[k],OUTPUT);

    pid[k].setParams(5,0.1,10,255);

#ifdef EMULATION
    posEmul[k] = 0;
#endif
  }

  

  // initialisation des positions et vitesses mémoire
  position.x = 200;
  position.y = 200;
  position.alpha = 0;
  vitesse.x = 0;
  vitesse.y = 0;
  vitesse.w = 0;

  accel.x = 0;
  accel.y = 0;
  mode.local = 0;
  mode.commande = M_IDLE;
  
  // interruptions sur les encodeurs
  attachInterrupt(digitalPinToInterrupt(enca[0]),readEncoder<0>,RISING);
  attachInterrupt(digitalPinToInterrupt(enca[1]),readEncoder<1>,RISING);
  attachInterrupt(digitalPinToInterrupt(enca[2]),readEncoder<2>,RISING);
  attachInterrupt(digitalPinToInterrupt(enca[3]),readEncoder<3>,RISING);
  
}

/**********************/
/* FIN INITIALISATION */
/**********************/



/*********************/
/* BOUCLE PRINCIPALE */
/*********************/

void loop() {

  /*******************/
  /* Réception trame */
  /*******************/
  if (stringComplete) {

    switch(inputString[0][0]){

      // commande en vitesse directe
      case 'v':
      case 'V':
        mode.commande = M_VITESSE;
        commande_vitesse.x=inputString[2].toFloat();
        commande_vitesse.y=inputString[3].toFloat();
        commande_vitesse.w=inputString[4].toFloat();

        if(inputString[0][1] && (inputString[0][1] == 'l' || inputString[0][1] == 'L')){
          mode.local = 1;
          commande_vitesse.alpha=inputString[5].toFloat();
        }else{
          mode.local = 0;
          commande_vitesse.alpha=0;
        }
        break;

      // commande en vitesse filtrée
      case 'f':
      case 'F':
      
        mode.commande = M_VITESSE_FILTRE;
        commande_vitesse.x=inputString[2].toFloat();
        commande_vitesse.y=inputString[3].toFloat();
        commande_vitesse.w=inputString[4].toFloat();
        
        if(inputString[0][1] && (inputString[0][1] == 'l' || inputString[0][1] == 'L')){
          mode.local = 1;
          commande_vitesse.alpha=inputString[5].toFloat();
        }else{
          mode.local = 0;
          commande_vitesse.alpha=0;
        }
        break;

      // commande en position
      case 'p':
      case 'P':
        commande_position.x=inputString[2].toFloat();
        commande_position.y=inputString[3].toFloat();
        commande_position.alpha=inputString[4].toFloat();
        mode.commande = M_POSITION;
        mode.local = 0;


        UUID = inputString[1];

        break;

      // calibration position mémoire
      case 'c':
      case 'C':
        position.x=inputString[2].toFloat();
        position.y=inputString[3].toFloat();
        position.alpha=inputString[4].toFloat();
        break;

      // arrêt de la base
      case 's':
      case 'S':
        mode.commande = M_IDLE;
        break;
      case 'r':
      case 'R':
        send_ins(4, inputString[2].toInt());
        break;

      // requête de la position de la base
      case 'd':
      case 'D':
        Serial.print("D;");
        Serial.print(inputString[1]);
        Serial.print(";");
        Serial.print(position.x);
        Serial.print(";");
        Serial.print(position.y);
        Serial.print(";");
        Serial.print(position.alpha);
        Serial.print("\n");
        break;

      // Modification des valeurs d'accélération et de vitesse max
      case 'B':{
        float vitesse = inputString[2].toFloat(); //convert string to float
        float accel = inputString[3].toFloat();
        changeParams(vitesse, accel);
        }
        break;
      
      // Modification des constantes de la base
      case 'A':{
        float r = inputString[2].toFloat(); //convert string to float
        float l= inputString[3].toFloat();
        changeParams2(r, l);
        }
        break;
        
      // Indication de la présence d'un obstacle
      case 'O':{
        int nb_obstacles = inputString[2].toFloat();
        int nb_obstacles_final = 0;
        TPosition obs[6];

        float ang, dist, x, y;
        for(int i = 0; i < nb_obstacles; i++){
          ang = inputString[2*i+3].toFloat();
          dist = inputString[2*i+4].toFloat();
          x = position.x + dist*cos(ang + position.alpha);
          y = position.y + dist*sin(ang + position.alpha);
          if(isInside(x, y)){
            obs[nb_obstacles_final].x = x;
            obs[nb_obstacles_final].y = y;
            nb_obstacles_final++;
          }
        }
        changeObstacles(nb_obstacles_final, obs);
        }
        break;
      case 'G':{
        send_ins(0, inputString[2].toInt());
        send_ins(1, inputString[3].toInt());
        send_ins(2, inputString[4].toInt());
        send_ins(3, inputString[5].toInt());
        }
        break;
      case 'E':{
        if(inputString[2][0] == '1')
          ESC.writeMicroseconds(ESC_ON);
        else
          ESC.writeMicroseconds(ESC_OFF);
        }
        break;
      case 'T':{
        if(inputString[2][0] == '1')
          servo_trappe.write(TRAPPE_OUVERT);
        else
          servo_trappe.write(TRAPPE_FERMEE);
        }
        break;
      default:
        // Serial.print("ERROR:INVALID_COMMAND:");
        // Serial.print(inputString[0]);
        // Serial.print(inputString[1]);
        // Serial.print(inputString[2]);
        // Serial.print(inputString[3]);
        // Serial.print(inputString[4]);
        // Serial.println();
        break;
    }

    // réinitialisation de la trame mémoire
    for(int i=0;i<=(NB_MOT+1);i++){
      inputString[i]="";
    }
    numero_string = 0;
    stringComplete = false;
  }

  /***********************/
  /* FIN Réception trame */
  /***********************/
  



  /************************************/
  /* Calcul de DT et des déplacements */
  /************************************/

  // DT
  long currT = micros();
  float deltaT = ((float) (currT - prevT))/( 1.0e6 );

  int pos[NMOTORS];
  
  // on sécurise la lecture des encodeurs, on désactive temporairement les interruptions
  noInterrupts(); 
  for(int k = 0; k < NMOTORS; k++){
    pos[k] = posi[k];
  }
  interrupts();

  float posDiff[4];
  for(int k = 0; k < NMOTORS; k++){
    posDiff[k] = pos[k] - posPrevi[k];
  }

  /****************************************/
  /* FIN Calcul de DT et des déplacements */
  /****************************************/




  /*************/
  /* Odométrie */
  /*************/

  float dx, dy, dAlpha;

  odometrieLocale(posDiff, &dx, &dy, &dAlpha);
  
  //moyenne de la variation de alpha pour calculer X et Y
  float alphaMoy = position.alpha + dAlpha/2; //moyenne de alpha sur l'intervalle de temps
  
  position.x += dx*cos(alphaMoy) - dy*sin(alphaMoy);
  position.y += dy*cos(alphaMoy) + dx*sin(alphaMoy);
  position.alpha += dAlpha;

  //limitation de alpha entre -PI et PI
  while(position.alpha >  PI) position.alpha -= TWO_PI;
  while(position.alpha < -PI) position.alpha += TWO_PI; 



  /*****************/
  /* FIN Odométrie */
  /*****************/



  /********************************/
  /* Interprétation de la commande*/
  /********************************/

  switch(mode.commande){
    case M_IDLE:
      target[0] = 0;
      target[1] = 0;
      target[2] = 0;
      target[3] = 0;
      
      vitesse.x = 0;
      vitesse.y = 0;
      vitesse.w = 0;
      break;

    case M_VITESSE:{
      if(mode.local == 1){
        calculCommandeAbsolu(commande_vitesse, commande_vitesse.alpha, target); // quand alpha est fixe par rapport au robot, on le prend dans la consigne en vitesse
      }else{
        calculCommandeAbsolu(commande_vitesse, position.alpha, target); 
      }
      vitesse.x = commande_vitesse.x;
      vitesse.y = commande_vitesse.y;
      vitesse.w = commande_vitesse.w;
    }
      break;

    case M_VITESSE_FILTRE:{
      float xFilt = commande_vitesse.x*coeffFiltre + vitesse.x*(1-coeffFiltre);
      float yFilt = commande_vitesse.y*coeffFiltre + vitesse.y*(1-coeffFiltre);
      float wFilt = commande_vitesse.w*coeffFiltre + vitesse.w*(1-coeffFiltre);



      if(mode.local == 1)
        calculCommandeAbsolu(xFilt, yFilt, wFilt, commande_vitesse.alpha, target);
      else
        calculCommandeAbsolu(xFilt, yFilt, wFilt, position.alpha, target);

      
      vitesse.x = xFilt;
      vitesse.y = yFilt;
      vitesse.w = wFilt;
      }
      break;

    case M_POSITION:
      arrivee = calculCommandePosition(commande_position, position, &vitesse, 1, 0.01, deltaT, target);
      if(arrivee){
        mode.commande = M_IDLE;
        Serial.print("P;");
        Serial.print(UUID);
        Serial.print(";Done\n");
      }
      break;

    default:

      break;
  }


  /************************************/
  /* FIN Interprétation de la commande*/
  /************************************/


  /**************************/
  /* Asservissements moteurs*/
  /**************************/

#ifdef EMULATION
  for(int k = 0; k < NMOTORS; k++){
    posEmul[k] += target[k] * (300.0 / 60.0) * deltaT;
    posi[k] = floor(posEmul[k]);
    posPrevi[k] = pos[k];
  }
#else
  // loop through the motors
  for(int k = 0; k < NMOTORS; k++){
    int pwr, dir;
    pid[k].setParams(5,0.1,10,255);
  
    float vCPS = (posDiff[k])/deltaT;
    posPrevi[k] = pos[k];

    //conversion en RPM
    float vRPM = vCPS/300.0*60.0;

    
    vFilt[k] = 0.981*vFilt[k] + 0.0093*vRPM + 0.0093*vPrev[k];
    vPrev[k] = vRPM;


    // evaluate the control signal
    pid[k].evalu(vFilt[k],target[k],deltaT,pwr,dir);
    // signal the motor
    setMotor(dir,pwr,pwm[k],in1[k],in2[k]);
  }
#endif
  prevT = currT;



  /******************************/
  /* FIN Asservissements moteurs*/
  /******************************/

  delay(2);  //défini la période entre chaque calcul
}



/*************************/
/* FIN BOUCLE PRINCIPALE */
/*************************/


// commande des moteurs
void setMotor(int dir, int pwmVal, int pwm, int in1, int in2){
  analogWrite(pwm,pwmVal);
  if(dir == 1){
    digitalWrite(in1,HIGH);
    digitalWrite(in2,LOW);
  }
  else if(dir == -1){
    digitalWrite(in1,LOW);
    digitalWrite(in2,HIGH);
  }
  else{
    digitalWrite(in1,LOW);
    digitalWrite(in2,LOW);
  }  
}

void send_ins(byte pile_ins, byte angle_ins){
  Wire.beginTransmission(slave_address);
  Wire.write(pile_ins);
  Wire.write(angle_ins);
  Wire.endTransmission();
}


// interruption de la réception d'une trame
void serialEvent() {
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read();
    // add it to the inputString:
    // if the incoming character is a newline, set a flag so the main loop can
    // do something about it:
    if (inChar == '\n') {
      stringComplete = true;
    }else{
      if(inChar == ';'){
        numero_string+=1;
      }else{
        inputString[numero_string] += inChar;
      }
    }
  }
}

// interruption encodeurs, utilisation de template
template <int j>
void readEncoder(){
  int b = digitalRead(encb[j]);
  if(b > 0){
    posi[j]++;
  }
  else{
    posi[j]--;
  }
}
