#include "odometrie.h"

#define MAX_OBSTACLES 6

float VITESSEMAX = 1000.0;
float ACCEL = 200.0;

float VITESSEANGMAX = 1;
float ACCELANG = 1;

float R_MM = 30.0;
float L_MM = 175.0;

float radius = L_MM + 200;
float safeZone = 5;

float A = 30*L_MM/(PI*R_MM);

float B = 60.0/(TWO_PI*R_MM);

float C = (PI*R_MM);
float D = C/(2*L_MM);

TPosition Obstacles[MAX_OBSTACLES];
uint8_t nb_obstacles = 0;


Obs_status obstacles_status[MAX_OBSTACLES];


// Modification des valeurs d'accélération et de vitesse
void changeParams(float v, float a){
  VITESSEMAX = v;
  ACCEL = a;
}

// Modification des valeurs de rayon des roues et de la base
void changeParams2(float r, float l){
  R_MM = r;
  L_MM = l;

  A = 30*L_MM/(PI*R_MM);
  
  B = 60.0/(TWO_PI*R_MM);
  C = (PI*R_MM);
  D = C/(2*L_MM);
  radius = L_MM + 200;
}

void changeObstacles(int nb,  TPosition *obs){
  nb_obstacles = nb;
  for(int i = 0; i < nb || i < MAX_OBSTACLES; i++){
    Obstacles[i].x = obs[i].x;
    Obstacles[i].y = obs[i].y;
  }
}

// odométrie locale
void odometrieLocale(float posDiff[], float *dx, float *dy, float *dAlpha){
  //conversion en rotation
  float rotation[4];
  for(int k = 0; k < NMOTORS; k++){
    rotation[k] = posDiff[k]/300.0;
  }
  *dx = (rotation[0] - rotation[2]) * C;
  *dy = (rotation[3] - rotation[1]) * C;
  *dAlpha = -(rotation[0] + rotation[1] + rotation[2] + rotation[3]) * D;
}



void calculCommandeAbsolu2(float Vitesse, float Angle, float w, float alpha, float target[]){

  float X = Vitesse*cos(Angle-alpha);
  float Y = Vitesse*sin(Angle-alpha);

  target[0] = B*( X) - w*A;
  target[1] = B*(-Y) - w*A;
  target[2] = B*(-X) - w*A;
  target[3] = B*( Y) - w*A;
}


void calculCommandeAbsolu(TVitesse consigne, float alpha, float target[]){
  calculCommandeAbsolu(consigne.x, consigne.y, consigne.w, alpha, target);
}
void calculCommandeAbsolu(float X, float Y, float w, float alpha, float target[]){

  //X et Y en mm/s, w en RPM, alpha en rad pour cos() et sin()
  //target[0] = B*( X*cos(alpha) + Y*sin(alpha)) - w*A;
  //target[1] = B*(-Y*cos(alpha) + X*sin(alpha)) - w*A;
  //target[2] = B*(-X*cos(alpha) - Y*sin(alpha)) - w*A;
  //target[3] = B*( Y*cos(alpha) - X*sin(alpha)) - w*A;

  //legere economie de calcul

  float Xcos = X*cos(alpha);
  float Xsin = X*sin(alpha);
  float Ycos = Y*cos(alpha);
  float Ysin = Y*sin(alpha);

  target[0] = B*( Xcos + Ysin) - w*A;
  target[1] = B*(-Ycos + Xsin) - w*A;
  target[2] = B*(-Xcos - Ysin) - w*A;
  target[3] = B*( Ycos - Xsin) - w*A;
}

bool doesIntersectCircle(TPosition a, TPosition b, TPosition c, float dist){

  float a_x = a.x - c.x;
  float a_y = a.y - c.y;

  float b_x = b.x - c.x;
  float b_y = b.y - c.y;

  float D = a_x*b_y - b_x*a_y;

  float delta = (radius * radius) * (dist * dist) - (D*D);

  if(delta >=0){
    //Serial.print("CIRCLE DOES INTERSECT\n");
    return true;
  }else{
    //Serial.print("CIRCLE DOESN'T INTERSECT\n");
    return false;
  }
}

bool projectionIsInside(TPosition a, TPosition b, TPosition c){
  float c_a_x = c.x - a.x;
  float c_a_y = c.y - a.y;
  
  float b_a_x = b.x - a.x;
  float b_a_y = b.y - a.y;




  float c_b_x = c.x - b.x;
  float c_b_y = c.y - b.y;
  
  float a_b_x = a.x - b.x;
  float a_b_y = a.y - b.y;

  if(c_a_x*b_a_x + c_a_y*b_a_y > 0 && c_b_x*a_b_x + c_b_y*a_b_y > 0){
    //Serial.print("PROJECTION IS INSIDE\n");
    return true;
  }else{
    //Serial.print("PROJECTION NOT INSIDE\n");
    return false;
  }


}

bool isInside(TPosition point){
  return isInside(point.x, point.y);
}
bool isInside(float x, float y){
  return x > L_MM && x < 3000 - L_MM && y > L_MM && y < 2000 - L_MM;
}

bool isTouchingNorthLimit(TPosition point){
  return point.y >= 2000 - L_MM - 20 - radius;
}
bool isTouchingSouthLimit(TPosition point){
  return point.y <= L_MM + 20 + radius;
}
bool isTouchingEastLimit(TPosition point){
  return point.x >= 3000 - L_MM - 20 - radius;
}
bool isTouchingWestLimit(TPosition point){
  return point.x <= L_MM + 20 + radius;
}

bool canBeReached(TPosition point){
  return isTouchingNorthLimit(point) && isTouchingSouthLimit(point) && isTouchingEastLimit(point) && isTouchingWestLimit(point);
}

bool isLeft(TPosition a, TPosition b, TPosition c){
  return ((b.x - a.x)*(c.y - a.y) - (b.y - a.y)*(c.y - a.y)) > 0;
}

float trim_angle(float a){
  while(a < 0) a += TWO_PI;
  while(a > TWO_PI) a -= TWO_PI;
  return a;
}

bool angleBetween(float n, float a, float b){
  n = trim_angle(n);
  a = trim_angle(a);
  b = trim_angle(b);
  if(a<b) return a <= n && n <= b;
  
  return a <= n || n <= b;
}

bool validPath(TPosition position, TPosition destination, TPosition t1, TPosition t2, TPosition t_robot, TPosition t_dest, int i){

  //we make sure the path if following an arc, and not a weird path made by two crossing tangents
  float angdiff = t2.alpha - t1.alpha;
  while(angdiff < -PI) angdiff += TWO_PI;
  while(angdiff > PI) angdiff -= TWO_PI;

  if(angdiff < 0){
    //Serial.print("PATH DENIED : NOT FOLLOWING AN ARC\n");
        //Serial.print("Coordinates : T1 : ");
        //Serial.print(t1.x);
        //Serial.print(", ");
        //Serial.print(t1.y);
        //Serial.print(", ");
        //Serial.print(t1.alpha);
        //Serial.print("\n");
        //Serial.print("T2 : ");
        //Serial.print(t2.x);
        //Serial.print(", ");
        //Serial.print(t2.y);
        //Serial.print(", ");
        //Serial.print(t2.alpha);
        //Serial.print("\n");
    return false;
  }

  //we first check if the tragectory doesn't take the robot out of bounds
  if(isTouchingNorthLimit(Obstacles[i])){
      //Serial.print("EDGE TOUCHING : ");
      //Serial.print(i);
      //Serial.print(" FROM NORTH\n");
    if(angleBetween(PI/2, t1.alpha, t2.alpha)){
      //Serial.print("EDGE BLOCKING : ");
      //Serial.print(i);
      //Serial.print(" FROM NORTH\n");
      return false;
    }
  }
  if(isTouchingSouthLimit(Obstacles[i])){
      //Serial.print("EDGE TOUCHING : ");
      //Serial.print(i);
      //Serial.print(" FROM SOUTH\n");
    if(angleBetween(-PI/2, t1.alpha, t2.alpha)){
      //Serial.print("EDGE BLOCKING : ");
      //Serial.print(i);
      //Serial.print(" FROM SOUTH\n");
      return false;
    }
  }
  if(isTouchingEastLimit(Obstacles[i])){
      //Serial.print("EDGE TOUCHING : ");
      //Serial.print(i);
      //Serial.print(" FROM EAST\n");
    if(angleBetween(0, t1.alpha, t2.alpha)){
      //Serial.print("EDGE BLOCKING : ");
      //Serial.print(i);
      //Serial.print(" FROM EAST\n");
      return false;
    }
  }
  if(isTouchingWestLimit(Obstacles[i])){
      //Serial.print("EDGE TOUCHING : ");
      //Serial.print(i);
      //Serial.print(" FROM WEST\n");
    if(angleBetween(PI, t1.alpha, t2.alpha)){
      //Serial.print("EDGE BLOCKING : ");
      //Serial.print(i);
      //Serial.print(" FROM WEST\n");
      return false;
    }
  }

  // then, we check if the path isn't obstructed by another obstacle's safety radius


  float dx, dy, dist, teta;
  for(int j = 0; j < nb_obstacles; j++){
    if(i == j) continue;
    dx = Obstacles[j].x - Obstacles[i].x;
    dy = Obstacles[j].y - Obstacles[i].y;
    dist = sqrt(dx*dx+dy*dy);
    if(dist < 2 * radius){
      teta = atan2(dy, dx);
      if(angleBetween(teta, t1.alpha, t2.alpha)){
        if(obstacles_status[i] != FULLYBLOCKED) obstacles_status[i] = BLOCKING;
        if(obstacles_status[j] != FULLYBLOCKED) obstacles_status[j] = BLOCKING;

        //Serial.print("BLOCKING : ");
        //Serial.print(j);
        //Serial.print(" is obstructing ");
        //Serial.print(i);
        //Serial.print("\n");
        //Serial.print("Coordinates : T1 : ");
        //Serial.print(t1.x);
        //Serial.print(", ");
        //Serial.print(t1.y);
        //Serial.print(", ");
        //Serial.print(t1.alpha);
        //Serial.print("\n");
        //Serial.print("T2 : ");
        //Serial.print(t2.x);
        //Serial.print(", ");
        //Serial.print(t2.y);
        //Serial.print(", ");
        //Serial.print(t2.alpha);
        //Serial.print("\nTEST AVEC TETA = ");
        //Serial.print(teta);
        //Serial.print("\n");
        return false;
      }else{
        dx = t_robot.x - position.x;
        dy = t_robot.y - position.y;
        dist = sqrt(dx*dx+dy*dy);
        if(doesIntersectCircle(position, t_robot, Obstacles[j], dist) && projectionIsInside(position, t_robot, Obstacles[j])){
          //Serial.print("BLOCKING PATH : ");
          //Serial.print(j);
          //Serial.print(" is obstructing path from robot to tangent of");
          //Serial.print(i);
          //Serial.print("\n");
          return false;
        }

        dx = t_dest.x - destination.x;
        dy = t_dest.y - destination.y;
        dist = sqrt(dx*dx+dy*dy);
        if(doesIntersectCircle(destination, t_dest, Obstacles[j], dist) && projectionIsInside(destination, t_dest, Obstacles[j])){
          //Serial.print("BLOCKING PATH : ");
          //Serial.print(j);
          //Serial.print(" is obstructing path from tangent of");
          //Serial.print(i);
          //Serial.print(" to destination\n");
          return false;
        }
      }
    }
    
  }


  //if the path checks all the tests, it is a valid path
  return true;
}

bool getTangents(TPosition c, TPosition p, TPosition & T1, TPosition & T2){

  float dx_obs = p.x - c.x;
  float dy_obs = p.y - c.y;

  float dist_obs = sqrt(dx_obs*dx_obs + dy_obs*dy_obs);
  float teta = atan2(dy_obs, dx_obs);

  if(dist_obs < radius){ //  the point is inside the circle, we return the closest point from the circle
    T1.x = c.x + radius*cos(teta);
    T1.y = c.y + radius*sin(teta);
    return false;
  }else{
    float alpha = acos(radius/dist_obs);


    T1.x = c.x + (radius + safeZone)*cos(teta+alpha);
    T1.y = c.y + (radius + safeZone)*sin(teta+alpha);
    T1.alpha = teta+alpha;

    T2.x = c.x + (radius + safeZone)*cos(teta-alpha);
    T2.y = c.y + (radius + safeZone)*sin(teta-alpha);
    T2.alpha = teta-alpha;

  return true;

   
  }

}

PathStatus getPath(TPosition position, TPosition destination, TPosition & target, bool check_blocking){

  if(check_blocking) {
    //Serial.print("BEGINNING PATHTRACING : 2ND WAVE\n");
  }

  float dx = destination.x - position.x;
  float dy = destination.y - position.y;
  float dist = sqrt(dx*dx + dy*dy);

  target.x = destination.x;
  target.y = destination.y;

  bool found = false, obstructed = false;

  for(int i = 0; i < nb_obstacles; i++){
    //Serial.print("OBSTACLE ");
    //Serial.print(i);
    //Serial.print(" : ");
    //Serial.print(Obstacles[i].x);
    //Serial.print(", ");
    //Serial.print(Obstacles[i].y);
    //Serial.print("\n");
    if(check_blocking && obstacles_status[i] != BLOCKING){
      //Serial.print("OBSTACKE ISN'T BLOCKING\n");
      continue;
    }// we doesn't check obstacles that are non-blocking


    if(check_blocking || (doesIntersectCircle(position, destination, Obstacles[i], dist) && projectionIsInside(position, destination, Obstacles[i]))){
      //Serial.print("INTERSECTION\n");
      obstructed = true;

      TPosition T1, T2, T3, T4;

      bool status;
      status = getTangents(Obstacles[i], position, T1, T2);
      if(!status){ // we are inside of a safety radius, we need to flee the obstacle
        target.x = T1.x;
        target.y = T1.y;
        //Serial.print("FOUND FORCED : ");
        //Serial.print(target.x);
        //Serial.print(", ");
        //Serial.print(target.y);
        //Serial.print("\n");
        return FOUND_FORCED; // we have found a forced path
      }
      //from there, we have two tangents coming from the robot
      status = getTangents(Obstacles[i], destination, T3, T4);
      if(!status){ // the target is inside a safety radius, we cannot go there
        //Serial.print("BLOCKED !!!!!");
        return BLOCKED;
      }
      
      //Serial.print("T1 : ");
      //Serial.print(T1.x);
      //Serial.print(", ");
      //Serial.print(T1.y);
      //Serial.print(", ");
      //Serial.print(T1.alpha);
      //Serial.print("\n");
      //Serial.print("T2 : ");
      //Serial.print(T2.x);
      //Serial.print(", ");
      //Serial.print(T2.y);
      //Serial.print(", ");
      //Serial.print(T2.alpha);
      //Serial.print("\n");
      //Serial.print("T3 : ");
      //Serial.print(T3.x);
      //Serial.print(", ");
      //Serial.print(T3.y);
      //Serial.print(", ");
      //Serial.print(T3.alpha);
      //Serial.print("\n");
      //Serial.print("T4 : ");
      //Serial.print(T4.x);
      //Serial.print(", ");
      //Serial.print(T4.y);
      //Serial.print(", ");
      //Serial.print(T4.alpha);
      //Serial.print("\n");

      //now, we have our 4 tangents, and we need to select them


      if(!isLeft(position, destination, Obstacles[i])){
        //la position la plus courte est à gauche, on check si elle est possible
        if(validPath(position, destination, T3, T2, T2, T3, i)){
          target.x = T2.x;
          target.y = T2.y;
          found = true;
          //Serial.print("FOUND T2 : ");
          //Serial.print(target.x);
          //Serial.print(", ");
          //Serial.print(target.y);
          //Serial.print("\n");
        }
        else if(validPath(position, destination, T1, T4, T1, T4, i)){
          target.x = T1.x;
          target.y = T1.y;
          //Serial.print("T2 OBSTRUCTED, FOUND T1 : ");
          //Serial.print(target.x);
          //Serial.print(", ");
          //Serial.print(target.y);
          //Serial.print("\n");
          found = true;
        } else {
          //Serial.print("T2 AND T1 OBSTRUCTED\n");
        }
      }else{
        //la position la plus courte est à droite, on check si elle est possible
        if(validPath(position, destination, T1, T4, T1, T4, i)){
          target.x = T1.x;
          target.y = T1.y;
          //Serial.print("FOUND T1 : ");
          //Serial.print(target.x);
          //Serial.print(", ");
          //Serial.print(target.y);
          //Serial.print("\n");
          found = true;
        }
        else if(validPath(position, destination, T3, T2, T2, T3, i)){
          target.x = T2.x;
          target.y = T2.y;
          //Serial.print("T1 OBSTRUCTED, FOUND T2 : ");
          //Serial.print(target.x);
          //Serial.print(", ");
          //Serial.print(target.y);
          //Serial.print("\n");
          found = true;
        } else {
          //Serial.print("T2 AND T1 OBSTRUCTED\n");
        }
      }

    }
  }
  //Serial.print("RESULT : obstructed : ");
  //Serial.print(obstructed);
  //Serial.print(", found : ");
  //Serial.print(found);
  //Serial.print("\n");

  if(!obstructed) return FOUND; // the direct line isn't obstructed, so the path is this direct line
  else if(found) return FOUND; // the path is obstructed, but a path is found

  //from there we need to search through the obstacles again
  if(!check_blocking) return getPath(position, destination, target, true);

  return NOTFOUND;
}

int calculCommandePosition(TPosition commande_position, TPosition position, TVitesse *vitesse, float precision, float precisionAngle, float deltaT, float target[]){
  //position
  char arriveePos = 0;
  char arriveeAng = 0;

  // for(int i = 0; i < 4; i++){
  //   obstacles_status[i] = NOTEXPLORED;
  // }

  // TPosition target_pos;

  //Serial.print("BEGINNING PATHTRACING\n");
  //Serial.print("POSITION  : ");
  //Serial.print(position.x);
  //Serial.print(", ");
  //Serial.print(position.y);
  //Serial.print("\n");
  // PathStatus result = getPath(position, commande_position, target_pos, false);
  //Serial.print("FINAL TARGET : ");
  //Serial.print(target_pos.x);
  //Serial.print(", ");
  //Serial.print(target_pos.y);
  //Serial.print("\n\n");

  // if(result == NOTFOUND){
  //   //Serial.print("NOT FOUND !!!!!!! \n");
  //   return -1;
  // }
  // if(result == BLOCKED){
  //   //Serial.print("BLOCKED !!!!!!! \n");
  //   return -1;
  // }



  float dir = 0;

  float dx = commande_position.x - position.x;
  float dy = commande_position.y - position.y;
  float dist = sqrt(dx*dx + dy*dy);
  if(dist > 0) dir = atan2(commande_position.y - position.y, commande_position.x - position.x);


  bool tooClose = false;
  for(int i = 0;i < nb_obstacles; i++){
    float dir_obs = 0;
    float dx_obs = Obstacles[i].x - position.x;
    float dy_obs = Obstacles[i].y - position.y;
    float dist_obs = sqrt(dx_obs*dx_obs + dy_obs*dy_obs);
    if(dist_obs > 0) dir_obs = atan2(dy_obs, dx_obs);

    float ang_diff = dir - dir_obs;

    while(ang_diff < -PI){
      ang_diff += TWO_PI;
    }
    while(ang_diff > PI){
      ang_diff -= TWO_PI;
    }

    if(dist_obs < L_MM + 400 && abs(ang_diff) < PI/2){
      tooClose = true;
      break;
    };
  }






  float magVitesse = sqrt(vitesse->x*vitesse->x + vitesse->y*vitesse->y);

  float vitesseDesiree = magVitesse + CONSTDEBUT;


  float deccelDistance = magVitesse*magVitesse/(2*ACCEL);

  if(tooClose == true){
    vitesseDesiree *= STOP_FACTOR;
  }else if(dist<precision){
    vitesseDesiree = 0;
    arriveePos = 1;
  }else if(dist < deccelDistance){
    vitesseDesiree -= ACCEL*deltaT;
  }else{
    vitesseDesiree += ACCEL*deltaT;
  }
  if(vitesseDesiree > VITESSEMAX) vitesseDesiree = VITESSEMAX;
  else if(vitesseDesiree<0) vitesseDesiree = 0;

    



  //angle (a revoir)
  float dAlpha = commande_position.alpha - position.alpha;
  if(dAlpha >  PI) dAlpha -= TWO_PI;
  if(dAlpha < -PI) dAlpha += TWO_PI;
  
  float angDiff = abs(dAlpha);
  float sign = (dAlpha>=0)?1:-1;

  float vitesseAngDesiree = abs(vitesse->w);

  float deccelAng = vitesseAngDesiree*vitesseAngDesiree/(2*ACCELANG);

  if(angDiff<precisionAngle){
    vitesseAngDesiree = 0;
    arriveeAng = 1;
  }else if(angDiff < deccelAng){
    vitesseAngDesiree -= ACCELANG*deltaT;
  }else{
    vitesseAngDesiree += ACCELANG*deltaT;
  }

  if(vitesseAngDesiree > VITESSEANGMAX) vitesseAngDesiree = VITESSEANGMAX;
  else if(vitesseAngDesiree<0) vitesseAngDesiree = 0;

  calculCommandeAbsolu2(vitesseDesiree + CONSTDEBUT, dir, sign*vitesseAngDesiree, position.alpha, target);

  vitesse->x = vitesseDesiree*cos(dir);
  vitesse->y = vitesseDesiree*sin(dir);
  vitesse->w = sign*vitesseAngDesiree;

  return arriveePos & arriveeAng;
}; 